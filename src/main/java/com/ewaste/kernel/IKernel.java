package com.ewaste.kernel;

public interface IKernel {

    /**
     * This function is used by the client. Implement computing here.
     * @param input input data bytes
     * @return result is output to be sent back to server
     */
    byte[] execute(byte[] input);
}
